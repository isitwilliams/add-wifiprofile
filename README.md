# Add-WifiProfile

Powershell Module for Adding Wifi Profiles to Windows Workstations

## Usage

`(New-Object Net.WebClient).DownloadString("https://gitlab.com/isitwilliams/add-wifiprofile/-/raw/main/Add-WifiProfile.psm1") | iex; Add-WifiProfile -SSID "Some WLAN Name" -PSK "Your Passphrase here"`



